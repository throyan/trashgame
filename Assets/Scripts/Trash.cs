﻿using System.Collections;
using UnityEngine;

namespace TrashGame {
	public class Trash : MonoBehaviour {
		public TrashType type;
		public float radius;
		public float flyingSpeed = 1f;

		public static event TrashEvent Recycled;

		public void FlyToTrashCan(TrashCan trashCan) {
			GetComponent<Collider>().enabled = false;
			GetComponent<Rigidbody>().isKinematic = true;

			StartCoroutine(Flying(trashCan));
		}

		protected IEnumerator Flying(TrashCan trashCan) {
			var moveVector = trashCan.trashTarget.position - transform.position;
			while (moveVector.magnitude > 1f) {
				float speed = Mathf.Min(moveVector.magnitude, flyingSpeed * Time.deltaTime);

				transform.position += moveVector.normalized * speed;

				moveVector = trashCan.trashTarget.position - transform.position;

				yield return null;
			}

			trashCan.Recycle(this);

			if (Recycled != null) {
				Recycled(this);
			}
		}
	}
}