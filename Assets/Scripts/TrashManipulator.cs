﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace TrashGame {
	public class TrashManipulator : MonoBehaviour {
		public Camera mainCamera;
		public LayerMask trashLayer;
		public LayerMask boundsLayer;
		public LayerMask trashCansLayer;
		public float rayDistance = 20f;
		public float movementSpeed = 5f;
		public Vector2 mouseHorizontalLimits = new Vector2(0.1f, 0.9f);
		public Vector2 mouseVerticalLimits = new Vector2(0.4f, 0.9f);

		public UnityEvent TrashPickedUp;
		public UnityEvent TrashDroppedOnTrashCan;

		protected TrashCan[] trashCans;
		protected Trash grabbedTrash;
		protected float distanceToGrabbedTrash;

		protected Vector2 mouseOnScreenHorizontalLimits;
		protected Vector2 mouseOnScreenVerticalLimits;

		protected void Awake() {
			trashCans = FindObjectsOfType<TrashCan>();

			OnValidate();
		}

		protected void OnEnable() {
			for (int i = 0; i < trashCans.Length; i++) {
				if (trashCans[i].GetComponent<Animation>().isPlaying) {
					enabled = false;
					StartCoroutine(WaitForTrashCansAnimation());

					return;
				}
			}
		}

		protected void OnValidate() {
			if (mainCamera == null) {
				mainCamera = FindObjectOfType<Camera>();
			}

			// @warning this values should be recalculated
			// when screen resolution changes
			// no need for mobile (?)
			mouseOnScreenHorizontalLimits = new Vector2(
				mouseHorizontalLimits.x * mainCamera.pixelWidth,
				mouseHorizontalLimits.y * mainCamera.pixelWidth
			);
			mouseOnScreenVerticalLimits = new Vector2(
				mouseVerticalLimits.x * mainCamera.pixelHeight,
				mouseVerticalLimits.y * mainCamera.pixelHeight
			);
		}

		protected void Update() {
			if (! Input.GetButton("Fire1")) {
				// trash released
				if (grabbedTrash != null) {
					ReleaseTrash(grabbedTrash);
					grabbedTrash = null;
				}

				return;
			}
			RaycastHit hit;
			var mousePosition = GetMousePosition();
			var cameraRay = mainCamera.ScreenPointToRay(mousePosition);

			// holding trash
			if (grabbedTrash != null) {

				// checking distance to bounds
				if (Physics.Raycast(cameraRay, out hit, rayDistance, boundsLayer.value)) {
					distanceToGrabbedTrash = (hit.point - cameraRay.origin).magnitude;
				}

				grabbedTrash.transform.position = Vector3.Lerp(
					grabbedTrash.transform.position,
					cameraRay.origin + cameraRay.direction * (distanceToGrabbedTrash - grabbedTrash.radius),
					Time.deltaTime * movementSpeed
				);

				return;
			}

			// checking for raycast
			if (Physics.Raycast(cameraRay, out hit, rayDistance, trashLayer.value)) {
				grabbedTrash = hit.transform.GetComponentInParent<Trash>();
				if (grabbedTrash == null) {
					return;
				}

				// disable gravity when moving
				// and make it really heavy
				// so it won't be pushed by anothers
				var rb = grabbedTrash.GetComponent<Rigidbody>();
				rb.mass = 1000;
				rb.useGravity = false;
				distanceToGrabbedTrash = (hit.point - cameraRay.origin).magnitude;

				for (int i = 0; i < trashCans.Length; i++) {
					if (trashCans[i].trashType == grabbedTrash.type) {
						trashCans[i].Open();
					}
				}

				if (TrashPickedUp != null) {
					TrashPickedUp.Invoke();
				}

				return;
			}
		}

		protected Vector3 GetMousePosition() {
			var mousePosition = Input.mousePosition;

			// do not limit mouse position when grabbing trash
			if (grabbedTrash == null) {
				return mousePosition;
			}

			if (mousePosition.x < mouseOnScreenHorizontalLimits.x) {
				mousePosition.x = mouseOnScreenHorizontalLimits.x;
			}
			if (mousePosition.x > mouseOnScreenHorizontalLimits.y) {
				mousePosition.x = mouseOnScreenHorizontalLimits.y;
			}
			if (mousePosition.y < mouseOnScreenVerticalLimits.x) {
				mousePosition.y = mouseOnScreenVerticalLimits.x;
			}
			if (mousePosition.y > mouseOnScreenVerticalLimits.y) {
				mousePosition.y = mouseOnScreenVerticalLimits.y;
			}

			return mousePosition;
		}

		protected void ReleaseTrash(Trash trash) {
			var mousePosition = GetMousePosition();
			var cameraRay = mainCamera.ScreenPointToRay(mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(cameraRay, out hit, rayDistance, trashCansLayer.value)) {
				var trashCan = hit.transform.GetComponentInParent<TrashCan>();
				if (trashCan != null && trashCan.trashType == trash.type) {
					trash.FlyToTrashCan(trashCan);

					if (TrashDroppedOnTrashCan != null) {
						TrashDroppedOnTrashCan.Invoke();
					}

					return;
				}
			}

			var rb = trash.GetComponent<Rigidbody>();
			rb.useGravity = true;
			rb.mass = 1;

			var targetPosition = cameraRay.origin + cameraRay.direction * (distanceToGrabbedTrash - trash.radius);
			var force = targetPosition - trash.transform.position;
			force.z = 0;
			rb.AddForce(force * movementSpeed, ForceMode.Impulse);

			for (int i = 0; i < trashCans.Length; i++) {
				if (trashCans[i].trashType == grabbedTrash.type) {
					trashCans[i].Close();
				}
			}
		}

		protected IEnumerator WaitForTrashCansAnimation() {
			for (int i = 0; i < trashCans.Length; i++) {
				var canAnimation = trashCans[i].GetComponent<Animation>();
				yield return new WaitWhile(() => canAnimation.isPlaying);
			}

			enabled = true;
		}
	}
}