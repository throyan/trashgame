﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace TrashGame {
	[RequireComponent(typeof(Animation))]
	public class ArrivingAnimation : MonoBehaviour {
		public UnityEvent Arrived;
		public UnityEvent Finished;

		public Vector3 appearingRotation = new Vector3(0, -90, 30);
		public float speed = 1;
		public float appearingDistance = 20;
		public float rotationSpeed = 1;

		protected Vector3 targetPosition;
		protected Quaternion targetRotation;

		protected void Start() {
			targetPosition = transform.position;
			targetRotation = transform.rotation;
			transform.eulerAngles = appearingRotation;

			transform.Translate(- Vector3.forward * appearingDistance);

			StartCoroutine(Appearing());
		}

		protected IEnumerator Appearing() {
			var directionToTarget = targetPosition - transform.position;
			var distance = directionToTarget.magnitude;
			var currentSpeed = Mathf.Min(speed, distance * 2);
			while (distance > 0.1f) {
				transform.position += directionToTarget * Time.deltaTime * currentSpeed;

				yield return null;

				directionToTarget = targetPosition - transform.position;
				distance = directionToTarget.magnitude;
				currentSpeed = Mathf.Min(speed, distance * 2);
				directionToTarget.Normalize();
			}

			if (Arrived != null) {
				Arrived.Invoke();
			}

			float t = 0;
			Quaternion rotation = transform.rotation;
			while (t <= 1) {
				transform.rotation = Quaternion.Lerp(rotation, targetRotation, t);
				t += Time.deltaTime * rotationSpeed;

				yield return null;
			}

			if (Finished != null) {
				Finished.Invoke();
			}
		}

	}
}