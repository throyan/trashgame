﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace TrashGame {
	public class TrashSpawner : MonoBehaviour {
		public int trashCount = 10;
		public Vector3 positionRange = Vector3.one;
		public float delay = 0.2f;

		public Transform container;
		public Trash trashPrefab;

		public GameObject[] foodModels = new GameObject[0];
		public GameObject[] glassModels = new GameObject[0];
		public GameObject[] paperModels = new GameObject[0];
		public GameObject[] plasticModels = new GameObject[0];

		protected GameObject[] trashModels;

		protected void Awake() {
			trashModels = foodModels.Concat(glassModels)
				.Concat(paperModels).Concat(plasticModels).ToArray();
		}

		protected IEnumerator Start() {
			for (int i = 0; i < trashCount; i++) {
				Spawn();

				yield return new WaitForSeconds(delay);
			}
		}

		protected void Update() {
			if (Input.GetKeyDown(KeyCode.Space)) {
				Spawn();
			}
		}

		protected void Spawn() {
			if (trashModels.Length == 0) {
				return;
			}
			var trash = Instantiate<Trash>(
				trashPrefab,
				container.position + new Vector3(
					Random.Range(-positionRange.x, positionRange.x),
					Random.Range(-positionRange.y, positionRange.y),
					Random.Range(-positionRange.z, positionRange.z)
				),
				Random.rotation,
				container
			);

			int index = Random.Range(0, trashModels.Length);
			TrashType type = GetTrashType(trashModels[index]);

			var model = Instantiate<GameObject>(
				trashModels[index],
				trash.transform.position,
				trash.transform.rotation,
				trash.transform
			);

			trash.type = type;

			var trashCollider = trash.GetComponent<MeshCollider>();
			trashCollider.sharedMesh = model.GetComponent<MeshFilter>().sharedMesh;

			trash.radius = Mathf.Max(
				trashCollider.bounds.size.x,
				trashCollider.bounds.size.y,
				trashCollider.bounds.size.z
			);
		}

		protected TrashType GetTrashType(GameObject model) {
			if (foodModels.Contains(model)) {
				return TrashType.Food;
			}
			if (glassModels.Contains(model)) {
				return TrashType.Glass;
			}
			if (paperModels.Contains(model)) {
				return TrashType.Paper;
			}
			if (plasticModels.Contains(model)) {
				return TrashType.Plastic;
			}

			throw new System.Exception("Can not determine trash type for " + model.name);
		}
	}
}