﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace TrashGame {
	[RequireComponent(typeof(Animation))]
	public class TrashCan : MonoBehaviour {

		public TrashType trashType;

		public AnimationClip OpenAnimation;
		public AnimationClip CloseAnimation;

		public Transform trashTarget;

		public UnityEvent Opened;
		public UnityEvent Closed;
		public UnityEvent RecycledTrash;

		const string OPEN_ANIMATION_NAME = "BinOpen";
		const string CLOSE_ANIMATION_NAME = "BinClose";

		protected Animation animationComponent;
		protected AnimationState openAnimationState;
		protected AnimationState closeAnimationState;

		protected float minAnimationTime;

		protected void Awake() {
			animationComponent = GetComponent<Animation>();

			animationComponent.AddClip(OpenAnimation, OPEN_ANIMATION_NAME);
			animationComponent.AddClip(CloseAnimation, CLOSE_ANIMATION_NAME);

			openAnimationState = animationComponent[OPEN_ANIMATION_NAME];
			closeAnimationState = animationComponent[CLOSE_ANIMATION_NAME];

			minAnimationTime = Mathf.Min(openAnimationState.length, closeAnimationState.length);
		}

		public void Open() {
			if (closeAnimationState.normalizedTime > 0 && closeAnimationState.normalizedTime < 1) {
				openAnimationState.time = Mathf.Max(0, minAnimationTime - closeAnimationState.time);
			}
			animationComponent.Play(OPEN_ANIMATION_NAME);

			if (Opened != null) {
				Opened.Invoke();
			}
		}

		public void Close() {
			if (openAnimationState.normalizedTime > 0 && openAnimationState.normalizedTime < 1) {
				closeAnimationState.time = Mathf.Max(0, minAnimationTime - openAnimationState.time);
			}
			animationComponent.Play(CLOSE_ANIMATION_NAME);

			if (Closed != null) {
				Closed.Invoke();
			}
		}

		public void OpenQueued() {
			StartCoroutine(PlayQueued(OPEN_ANIMATION_NAME, Open));
		}

		public void CloseQueued() {
			StartCoroutine(PlayQueued(CLOSE_ANIMATION_NAME, Close));
		}

		public void Recycle(Trash trash) {
			Destroy(trash.gameObject);
			Close();

			if (RecycledTrash != null) {
				RecycledTrash.Invoke();
			}
		}

		protected IEnumerator PlayQueued(string animationName, System.Action callback) {
			if (animationComponent.IsPlaying(animationName)) {
				yield break;
			}

			if (animationComponent.isPlaying) {
				yield return new WaitWhile(() => animationComponent.isPlaying);
			}

			callback.Invoke();

			yield return new WaitForEndOfFrame();
		}
	}
}