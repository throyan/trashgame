﻿using UnityEngine;
using UnityEngine.Events;

namespace TrashGame {
	public class GameManager : MonoBehaviour {
		public UnityEvent AreaCleared;

		void OnEnable() {
			Trash.Recycled += CheckAreaCleared;
		}

		void OnDisable() {
			Trash.Recycled -= CheckAreaCleared;
		}

		void Update() {
			if (Input.GetKeyDown(KeyCode.R)) {
				// restart
				UnityEngine.SceneManagement.SceneManager.LoadScene(
					UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex
				);
			}
			if (Input.GetKeyDown(KeyCode.Escape)) {
				#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
				#else
				Application.Quit();
				#endif
			}
		}

		protected void CheckAreaCleared(Trash trash) {
			var allTrash = FindObjectsOfType<Trash>();
			if (allTrash.Length > 1 || allTrash[0] != trash) {
				return;
			}
			if (AreaCleared != null) {
				AreaCleared.Invoke();
			}
		}
	}

}